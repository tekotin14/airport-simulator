package factory;

import flight.Flight;
import flight.PassengerFlight;

/**
 * Created by grazi on 11.05.16.
 */
public class PassengerFlightFactory extends FlightFactory {

    protected Flight newInstanceOfFlight(int flightNumber, String flightType) {
        return new PassengerFlight(flightNumber, flightType);
    }

    protected int count() {
        return ++flightNumber;
    }
}
