package factory;

import flight.CargoFlight;
import flight.Flight;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Created by grazi on 11.05.16.
 */
public class CargoFlightFactoryTest {

    @Test
    public void shouldCreateNewInstanceOfFlight_A380() throws Exception {

        CargoFlightFactory factory = new CargoFlightFactory();

        Flight cargoFlight = factory.newInstanceOfFlight(1, "A380");

        assertThat(cargoFlight, is(notNullValue()));
        assertThat(cargoFlight, is(instanceOf(CargoFlight.class)));
    }

    @Test
    public void shouldCreateNewInstanceOfFlight_Dreamliner() throws Exception {

        CargoFlightFactory factory = new CargoFlightFactory();

        Flight cargoFlight = factory.newInstanceOfFlight(1, "Dreamliner");

        assertThat(cargoFlight, is(notNullValue()));
        assertThat(cargoFlight, is(instanceOf(CargoFlight.class)));
    }

}