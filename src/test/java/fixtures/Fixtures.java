package fixtures;

import flight.CargoFlight;
import flight.Flight;
import flight.PassengerFlight;
import person.CabinCrewMember;
import person.Passenger;
import person.Pilot;

/**
 * Created by grazi on 12.05.16.
 */
public class Fixtures {

    public static Flight getCargoA380(int flightNumber) {
        return new CargoFlight(flightNumber, "A380");
    }

    public static CargoFlight getCargoA380Status(int flightNumber, int status) {
        return new CargoFlightStatus(flightNumber, "A380", status);
    }

    public static CargoFlight getCargoA380StatusWithCargoStatus(int flightNumber, int status, int cargoStatus) {
        return new CargoFlightStatus(flightNumber, "A380", status, cargoStatus);
    }

    public static PassengerFlight getPassengerDreamliner(int flightNumber) {
        return new PassengerFlight(flightNumber, "Dreamliner");
    }

    public static PassengerFlight getPassengerDreamlinerStatus(int flightNumber, int status) {
        return new StatusFlight(flightNumber, "Dreamliner", status);
    }

    public static PassengerFlight getPassengerDreamlinerStatusWithPassenger(int flightNumber, int status, int anzPassengers) {
        return new StatusFlight(flightNumber, "Dreamliner", status, anzPassengers);
    }


    public static Pilot pilot = new Pilot("Hans Muster");
    public static CabinCrewMember member = new CabinCrewMember("Max Muster");
    public static CabinCrewMember member2 = new CabinCrewMember("Max Muster");
    public static CabinCrewMember member3 = new CabinCrewMember("Max Muster");
    public static CabinCrewMember member4 = new CabinCrewMember("Max Muster");
    public static CabinCrewMember member5 = new CabinCrewMember("Max Muster");
    public static Passenger passenger = new Passenger("Sandro Muster");


}
