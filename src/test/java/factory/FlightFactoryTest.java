package factory;

import flight.CargoFlight;
import flight.Flight;
import flight.PassengerFlight;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by grazi on 11.05.16.
 */
public class FlightFactoryTest {

    @Test
    public void shouldBuildANewAirplane_positiveNumber_passengerFlight() throws Exception {

        FlightFactory flightFactory = spy(new PassengerFlightFactory()) ;

        Flight flight = flightFactory.buildANewAirplane("A380");

        verify(flightFactory).newInstanceOfFlight(1, "A380");

        assertThat(flight, is(notNullValue()));
        assertThat(flight, is(CoreMatchers.instanceOf(PassengerFlight.class)));
        assertThat(flight.getStatus(), is(Flight.EMPTY_AIRPLANE));

        flight = flightFactory.buildANewAirplane("A380");

        verify(flightFactory).newInstanceOfFlight(2, "A380");

        assertThat(flight, is(notNullValue()));
        assertThat(flight, is(CoreMatchers.instanceOf(PassengerFlight.class)));
        assertThat(flight.getStatus(), is(Flight.EMPTY_AIRPLANE));

    }


    @Test
    public void shouldBuildANewAirplane_negativeNumber_cargoFlight() throws Exception {

        FlightFactory flightFactory = spy(new CargoFlightFactory()) ;

        Flight flight = flightFactory.buildANewAirplane("Dreamliner");

        verify(flightFactory).newInstanceOfFlight(-1, "Dreamliner");

        assertThat(flight, is(notNullValue()));
        assertThat(flight, is(CoreMatchers.instanceOf(CargoFlight.class)));
        assertThat(flight.getStatus(), is(Flight.EMPTY_AIRPLANE));

        flight = flightFactory.buildANewAirplane("dreamliner");

        verify(flightFactory).newInstanceOfFlight(-2, "dreamliner");

        assertThat(flight, is(notNullValue()));
        assertThat(flight, is(CoreMatchers.instanceOf(CargoFlight.class)));
        assertThat(flight.getStatus(), is(Flight.EMPTY_AIRPLANE));
    }


    @Test
    public void shouldNotBuildANewAirplane_isNotA380AndDreamliner_otherFlightType() throws Exception {

        FlightFactory flightFactory = spy(new CargoFlightFactory()) ;

        Flight flight = flightFactory.buildANewAirplane("Jumbo");

        verify(flightFactory, never()).newInstanceOfFlight(anyInt(), anyString());

    }

    @Test
    public void shouldNotBuildANewAirplane_isNotA380AndDreamliner_notFlightType() throws Exception {

        FlightFactory flightFactory = spy(new CargoFlightFactory()) ;

        Flight flight = flightFactory.buildANewAirplane("XAKD");

        verify(flightFactory, never()).newInstanceOfFlight(anyInt(), anyString());

    }

}