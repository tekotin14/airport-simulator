package flight;

import fixtures.Fixtures;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import person.Person;

import java.util.ArrayList;
import java.util.List;

import static fixtures.Fixtures.*;
import static flight.Flight.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by grazi on 15.05.16.
 */
public class FlightTest {

    @Test
    public void shouldBoardingFlightCrew() throws Exception {

        Flight flight = getPassengerDreamlinerStatus(1, Flight.EMPTY_AIRPLANE);

        List<Person> flightCrew = new ArrayList<Person>();
        flightCrew.add(pilot);
        flightCrew.add(member);
        flightCrew.add(member2);
        flightCrew.add(member3);
        flightCrew.add(member4);
        flightCrew.add(member5);

        flight.boardingFlightCrew(flightCrew);

        assertThat(flight.getStatus(), is(CABIN_CREW_ON_BOARD));
        assertThat(flight.getMembers().size(), is(5));
        assertThat(flight.getPilots().isEmpty(), is(false));

    }

    @Test
    public void shouldNotBoardingFlightCrew_notEnoughMembers() throws Exception {

        Flight flight = getPassengerDreamlinerStatus(1, Flight.EMPTY_AIRPLANE);

        List<Person> flightCrew = new ArrayList<Person>();
        flightCrew.add(pilot);
        flightCrew.add(member);
        flightCrew.add(member2);
        flightCrew.add(member3);
        flightCrew.add(member4);

        flight.boardingFlightCrew(flightCrew);

        assertThat(flight.getStatus(), is(EMPTY_AIRPLANE));
        assertThat(flight.getPilots().isEmpty(), is(true));
        assertThat(flight.getMembers().isEmpty(), is(true));

    }
    @Test
    public void shouldNotBoardingFlightCrew_noPilot() throws Exception {

        Flight flight = getPassengerDreamlinerStatus(1, Flight.EMPTY_AIRPLANE);

        List<Person> flightCrew = new ArrayList<Person>();

        flightCrew.add(member);
        flightCrew.add(member2);
        flightCrew.add(member3);
        flightCrew.add(member4);
        flightCrew.add(member5);

        flight.boardingFlightCrew(flightCrew);

        assertThat(flight.getStatus(), is(EMPTY_AIRPLANE));
        assertThat(flight.getPilots().isEmpty(), is(true));
        assertThat(flight.getMembers().isEmpty(), is(true));

    }



    @Test
    public void shouldTakeOff() throws Exception {
        Flight flight = getPassengerDreamlinerStatusWithPassenger(1, CABIN_CREW_ON_BOARD, 5);

        flight.takeOff();

        assertThat(flight.getStatus(), is(AIRPLANE_ON_AIR));
    }

    @Test
    public void shouldNotTakeOff_wrongStatus() throws Exception {
        Flight flight = getPassengerDreamlinerStatusWithPassenger(1, EMPTY_AIRPLANE, 5);

        flight.takeOff();

        assertThat(flight.getStatus(), is(EMPTY_AIRPLANE));
    }

    @Test
    public void shouldLand() throws Exception {
        Flight flight = getPassengerDreamlinerStatusWithPassenger(1, AIRPLANE_ON_AIR, 44);

        flight.land();

        assertThat(flight.getStatus(), is(AIRPLANE_LANDED));
    }

    @Test
    public void shouldNotLand_wrongStatus() throws Exception {
        Flight flight = getPassengerDreamlinerStatusWithPassenger(1, CABIN_CREW_ON_BOARD, 44);

        flight.land();

        assertThat(flight.getStatus(), is(CABIN_CREW_ON_BOARD));
    }

    @Test
    public void unloadFlightCrew() throws Exception {
        Flight flight = getPassengerDreamlinerStatus(1, AIRPLANE_LANDED);
        List<Person> flightCrew = new ArrayList<Person>();
        flightCrew.add(pilot);
        flightCrew.add(member);
        flightCrew.add(member2);
        flightCrew.add(member3);
        flightCrew.add(member4);
        flightCrew.add(member5);

        flight.boardingFlightCrew(flightCrew);
        List<Person> persons = flight.unloadFlightCrew();

        assertThat(flight.getPilots().isEmpty(), is(true));
        assertThat(flight.getMembers().isEmpty(), is(true));
        assertThat(flight.getStatus(), is(EMPTY_AIRPLANE));

    }

}