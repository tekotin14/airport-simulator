package airport;

import flight.CargoFlight;
import flight.Flight;
import flight.PassengerFlight;
import org.junit.Test;
import person.Passenger;
import person.RandomNameGenerator;

import java.util.List;

import static fixtures.Fixtures.*;
import static flight.CargoFlight.CARGO_LOADED;
import static flight.CargoFlight.CARGO_NOT_LOADED;
import static flight.Flight.*;
import static flight.PassengerFlight.PASSENGER_ON_BOARD;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by grazi on 11.05.16.
 */
public class AirportTest {
    @Test
    public void shouldSetAirportNameWhenCreateInstance() throws Exception {
        Airport airport = new Airport("Zürich", 1);

        String location = airport.getLocation();

        assertThat(location, is("Zürich"));
    }

    @Test
    public void shouldAcceptNewBuildedFlight() throws Exception {

        Airport airport = new Airport("Zürich", 2);

        Flight passengerDreamliner = getPassengerDreamliner(1);
        airport.acceptNewBuildedFlight(passengerDreamliner);

        assertThat(airport.getFlights(), is(contains(passengerDreamliner)));
    }

    @Test
    public void shouldNotAcceptNewBuildedFlight_statusNot0() throws Exception {

        Airport airport = new Airport("Zürich", 2);

        airport.acceptNewBuildedFlight(getPassengerDreamlinerStatus(1, CABIN_CREW_ON_BOARD));

        assertThat(airport.getFlights().isEmpty(), is(true));
    }

    @Test
    public void shouldNotAcceptBuildedFlightsWhenMaxCapacityIsReached() throws Exception {
        Airport airport = new Airport("Zürich", 1);

        Flight cargoA380 = getCargoA380(1);
        airport.acceptNewBuildedFlight(cargoA380);
        airport.acceptNewBuildedFlight(getPassengerDreamliner(2));

        assertThat(airport.getFlights(), is(contains(cargoA380)));
        assertThat(airport.getFlights().size(), is(1));
    }

    @Test
    public void shouldAcceptNewPilot() throws Exception {

        Airport airport = new Airport("Zürich", 2);

        airport.acceptNewPilot(pilot);

        assertThat(airport.getPilots(), is(contains(pilot)));

    }

    @Test
    public void shouldAcceptNewCabinCrewMember() throws Exception {
        Airport airport = new Airport("Zürich", 2);

        airport.acceptNewCabinCrewMember(member);

        assertThat(airport.getCabinCrewMembers(), is(contains(member)));
    }

    @Test
    public void shouldAcceptNewPassenger() throws Exception {
        Airport airport = new Airport("Zürich", 2);

        airport.acceptNewPassenger(passenger);

        assertThat(airport.getPassengers(), is(contains(passenger)));
    }

    @Test
    public void shouldGetFlightNumbersWithoutFlightCrew() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        airport.acceptNewBuildedFlight(getPassengerDreamliner(2));
        airport.acceptNewBuildedFlight(getPassengerDreamlinerStatus(1, CABIN_CREW_ON_BOARD));

        List<Integer> flightNumbersWithoutFlightCrew = airport.getFlightsWithoutFlightCrew();

        assertThat(flightNumbersWithoutFlightCrew, is(contains(2)));

    }

    @Test
    public void shouldGetFlightsWithoutFlightCrew_FlightNotEmpty() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        airport.acceptNewBuildedFlight(getPassengerDreamlinerStatus(1, CABIN_CREW_ON_BOARD));


        List<Integer> flightsWithoutFlightCrew = airport.getFlightsWithoutFlightCrew();

        assertThat(flightsWithoutFlightCrew.isEmpty(), is(true));

    }

    @Test
    public void shouldBoardingFlightCrew() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerDreamliner = getPassengerDreamliner(2);
        airport.acceptNewBuildedFlight(passengerDreamliner);
        addFlightCrewToAirport(airport);

        airport.boardingFlightCrew(2);

        assertThat(passengerDreamliner.getStatus(), is(Flight.CABIN_CREW_ON_BOARD));
        assertThat(airport.getPilots().isEmpty(), is(true));
        assertThat(airport.getCabinCrewMembers().isEmpty(), is(true));

    }

    @Test
    public void shouldBoardingFlightCrew_notEnoughMembers() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerDreamliner = getPassengerDreamliner(1);
        airport.acceptNewBuildedFlight(passengerDreamliner);
        airport.acceptNewPilot(pilot);
        airport.acceptNewCabinCrewMember(member);
        airport.acceptNewCabinCrewMember(member2);
        airport.acceptNewCabinCrewMember(member3);
        airport.acceptNewCabinCrewMember(member4);

        airport.boardingFlightCrew(2);

        assertThat(passengerDreamliner.getStatus(), is(Flight.EMPTY_AIRPLANE));
        assertThat(airport.getPilots().size(), is(1));
        assertThat(airport.getCabinCrewMembers().size(), is(4));

    }

    @Test
    public void shouldBoardingFlightCrew_notEnoughPilot() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerDreamliner = getPassengerDreamliner(1);
        airport.acceptNewBuildedFlight(passengerDreamliner);

        airport.acceptNewCabinCrewMember(member);
        airport.acceptNewCabinCrewMember(member2);
        airport.acceptNewCabinCrewMember(member3);
        airport.acceptNewCabinCrewMember(member4);
        airport.acceptNewCabinCrewMember(member5);


        airport.boardingFlightCrew(2);

        assertThat(passengerDreamliner.getStatus(), is(Flight.EMPTY_AIRPLANE));
        assertThat(airport.getPilots().isEmpty(), is(true));
        assertThat(airport.getCabinCrewMembers().size(), is(5));

    }

    @Test
    public void shouldGetCargoFlights4LoadCargo_onlyCargoFlights_withStatusReadyToLoad() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight cargoA380 = getCargoA380(1);
        airport.acceptNewBuildedFlight(cargoA380);

        addFlightCrewToAirport(airport);

        airport.boardingFlightCrew(1);

        List<Integer> flights4LoadCargo = airport.getCargoFlights4LoadCargo();

        assertThat(flights4LoadCargo.size(), is(1));
        assertThat(flights4LoadCargo.get(0), is(1));

        CargoFlight cargoFlight = (CargoFlight) cargoA380;

        assertThat(cargoFlight.getCargoStatus(), is(CargoFlight.CARGO_NOT_LOADED));
    }

    @Test
    public void shouldGetCargoFlights4LoadCargo_cargoFlight_noFlightCrew() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight cargoA380 = getCargoA380(1);
        airport.acceptNewBuildedFlight(cargoA380);

        airport.acceptNewPilot(pilot);
        airport.acceptNewCabinCrewMember(member);
        airport.acceptNewCabinCrewMember(member2);


        airport.boardingFlightCrew(1);

        List<Integer> flights4LoadCargo = airport.getCargoFlights4LoadCargo();

        assertThat(flights4LoadCargo.isEmpty(), is(true));

        CargoFlight cargoFlight = (CargoFlight) cargoA380;
        assertThat(cargoFlight.getStatus(), is(Flight.EMPTY_AIRPLANE));
        assertThat(cargoFlight.getCargoStatus(), is(CargoFlight.CARGO_NOT_LOADED));
    }

    @Test
    public void shouldLoadCargo4Flight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        addFlightCrewToAirport(airport);

        Flight cargoA380 = getCargoA380(1);
        airport.acceptNewBuildedFlight(cargoA380);

        airport.boardingFlightCrew(1);

        airport.loadCargo4Flight(1);

        CargoFlight cargoFlight = (CargoFlight) cargoA380;

        assertThat(cargoFlight.getCargoStatus(), is(CARGO_LOADED));
    }

    @Test
    public void shouldGetPassengerFlights4LoadPassenger() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerFlight = getPassengerDreamliner(2);
        airport.acceptNewBuildedFlight(passengerFlight);

        addFlightCrewToAirport(airport);

        airport.boardingFlightCrew(2);

        List<Integer> flightNumbers = airport.getPassengerFlights4LoadPassenger();

        assertThat(flightNumbers, is(contains(2)));

    }

    @Test
    public void shouldLoadPassenger4Flight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        PassengerFlight passengerDreamliner = spy(getPassengerDreamliner(2));

        airport.acceptNewBuildedFlight(passengerDreamliner);

        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);
        addPassengerToAirport(airport, 200);

        airport.loadPassenger4Flight(2, 100);

        assertThat(airport.getPassengers().size(), is(100));
        assertThat(passengerDreamliner.getPassengerStatus(), is(PASSENGER_ON_BOARD));
        verify(passengerDreamliner).boardingPassengers(anyList());

    }

    @Test
    public void shouldNotLoadPassenger4Flight_notCorrectStatus() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        PassengerFlight passengerDreamliner = spy(getPassengerDreamliner(2));

        airport.acceptNewBuildedFlight(passengerDreamliner);

        addPassengerToAirport(airport, 200);

        airport.loadPassenger4Flight(2, 100);

        assertThat(airport.getPassengers().size(), is(200));
        assertThat(passengerDreamliner.getStatus(), is(EMPTY_AIRPLANE));
        verify(passengerDreamliner, never()).boardingPassengers(anyList());
    }

    @Test
    public void shouldNotLoadPassenger4Flight_notEnoughPassengers() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        PassengerFlight passengerDreamliner = spy(getPassengerDreamliner(2));

        airport.acceptNewBuildedFlight(passengerDreamliner);

        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);
        addPassengerToAirport(airport, 99);

        airport.loadPassenger4Flight(2, 100);

        assertThat(airport.getPassengers().size(), is(99));
        assertThat(passengerDreamliner.getStatus(), is(CABIN_CREW_ON_BOARD));
        verify(passengerDreamliner, never()).boardingPassengers(anyList());

    }


    @Test
    public void shouldGetFlightsWithoutDestination() throws Exception {

        Airport airport = new Airport("Zürich", 2);
        Flight passengerFlight = getPassengerDreamliner(2);
        airport.acceptNewBuildedFlight(passengerFlight);

        addFlightCrewToAirport(airport);

        airport.boardingFlightCrew(2);

        addPassengerToAirport(airport, 200);

        airport.loadPassenger4Flight(2, 100);

        List<Integer> flightNumbers = airport.getFlightsWithoutDestination();

        assertThat(flightNumbers, is(contains(2)));

    }

    @Test
    public void shouldSetDestination() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerFlight = getPassengerDreamliner(2);
        airport.acceptNewBuildedFlight(passengerFlight);
        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);
        addPassengerToAirport(airport, 200);
        airport.loadPassenger4Flight(2, 100);

        Airport destAirport = new Airport("Honululu", 2);

        airport.setDestination(2, destAirport);

        assertThat(passengerFlight.getDestAirport(), is(destAirport));
    }

    @Test
    public void shouldSetDestination_cargoFlight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight cargoFlight = getCargoA380(2);
        airport.acceptNewBuildedFlight(cargoFlight);
        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);

        airport.loadCargo4Flight(2);

        Airport destAirport = new Airport("Honululu", 2);

        airport.setDestination(2, destAirport);

        assertThat(cargoFlight.getDestAirport(), is(destAirport));
    }

    @Test
    public void shouldSetNoDestination_flightWithoutPassengerOnBoard() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerFlight = getPassengerDreamliner(2);
        airport.acceptNewBuildedFlight(passengerFlight);
        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);

        Airport destAirport = new Airport("Honululu", 2);

        airport.setDestination(2, destAirport);

        assertThat(passengerFlight.getDestAirport(), is(nullValue()));
    }

    @Test
    public void shouldSetNoDestination_flightWithoutPassengerOnBoard_cargoFlight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerFlight = getCargoA380(2);
        airport.acceptNewBuildedFlight(passengerFlight);
        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);

        Airport destAirport = new Airport("Honululu", 2);

        airport.setDestination(2, destAirport);

        assertThat(passengerFlight.getDestAirport(), is(nullValue()));
    }

    @Test
    public void takeOffFlight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight cargoFlight = getCargoA380(2);
        airport.acceptNewBuildedFlight(cargoFlight);
        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);
        airport.loadCargo4Flight(2);
        Airport destAirport = new Airport("Honululu", 2);
        airport.setDestination(2, destAirport);

        airport.takeOffFlight(2);

        assertThat(airport.getFlights(), is(not(contains(cargoFlight))));
        assertThat(destAirport.getFlights(), is(contains(cargoFlight)));
        assertThat(cargoFlight.getStatus(), is(AIRPLANE_ON_AIR));
    }

    @Test
    public void getFlights4Landing() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight cargoFlight = getCargoA380(2);
        airport.acceptNewBuildedFlight(cargoFlight);
        addFlightCrewToAirport(airport);
        airport.boardingFlightCrew(2);
        airport.loadCargo4Flight(2);
        Airport destAirport = new Airport("Honululu", 2);
        airport.setDestination(2, destAirport);
        airport.takeOffFlight(2);

        List<Integer> flightNumbers = destAirport.getFlights4Landing();

        assertThat(flightNumbers, contains(2));
    }

    @Test
    public void shouldLandingFlight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight passengerFlight = getPassengerDreamlinerStatus(2, AIRPLANE_ON_AIR);
        airport.getFlights().add(passengerFlight);

        airport.landingFlight(2);

        assertThat(passengerFlight.getStatus(), is(AIRPLANE_LANDED));
    }

    @Test
    public void shouldGetFlights4GoToGate() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        Flight flight = getPassengerDreamlinerStatus(2, AIRPLANE_LANDED);
        airport.getFlights().add(flight);

        List<Integer> flightsNumber = airport.getFlights4GoToGate();

        assertThat(flightsNumber, is(contains(2)));
    }

    @Test
    public void shouldUnloadPassengers() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        PassengerFlight flight = spy (getPassengerDreamlinerStatusWithPassenger(2, AIRPLANE_LANDED, 88));
        airport.getFlights().add(flight);

        airport.unloadPassengers(2);

        assertThat(airport.getPassengers().size(), is(88));
        verify(flight).unloadPassengers();
    }

    @Test
    public void shouldGetFlights4GoToUnloadCargo() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        CargoFlight cargoFlight = getCargoA380StatusWithCargoStatus(2, AIRPLANE_LANDED, CARGO_LOADED);
        airport.getFlights().add(cargoFlight);

        List<Integer> flightsNumber = airport.getFlights4GoToUnloadCargo();

        assertThat(flightsNumber, is(contains(2)));
    }

    @Test
    public void shouldGetFlights4GoToUnloadCargo_cargoNotLoaded() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        CargoFlight cargoFlight = getCargoA380StatusWithCargoStatus(2, AIRPLANE_LANDED, CARGO_NOT_LOADED);
        airport.getFlights().add(cargoFlight);

        List<Integer> flightsNumbers = airport.getFlights4GoToUnloadCargo();

        assertThat(flightsNumbers.isEmpty(), is(true));
    }

    @Test
    public void shouldGetFlights4GoToUnloadFlightCrew_cargoFlight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        CargoFlight cargoFlight = getCargoA380StatusWithCargoStatus(2, AIRPLANE_LANDED, CARGO_NOT_LOADED);
        airport.getFlights().add(cargoFlight);

        List<Integer> flightsNumbers = airport.getFlights4GoToUnloadFlightCrew();

        assertThat(flightsNumbers, is(contains(2)));
    }

    @Test
    public void shouldGetFlights4GoToUnloadFlightCrew_cargoFlight_cargoIsLoaded() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        CargoFlight cargoFlight = getCargoA380StatusWithCargoStatus(2, AIRPLANE_LANDED, CARGO_LOADED);
        airport.getFlights().add(cargoFlight);

        List<Integer> flightsNumbers = airport.getFlights4GoToUnloadFlightCrew();

        assertThat(flightsNumbers.isEmpty(), is(true));
    }

    @Test
    public void shouldGetFlights4GoToUnloadFlightCrew_passengerFlight() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        PassengerFlight passengerFlight = getPassengerDreamlinerStatus(2, AIRPLANE_LANDED);
        airport.getFlights().add(passengerFlight);

        List<Integer> flightsNumbers = airport.getFlights4GoToUnloadFlightCrew();

        assertThat(flightsNumbers, is(contains(2)));
    }

    @Test
    public void shouldGetFlights4GoToUnloadFlightCrew_passengerFlight_withPassengers() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        PassengerFlight passengerFlight = getPassengerDreamlinerStatusWithPassenger(2, AIRPLANE_LANDED, 4);
        airport.getFlights().add(passengerFlight);

        List<Integer> flightsNumbers = airport.getFlights4GoToUnloadFlightCrew();

        assertThat(flightsNumbers.isEmpty(), is(true));
    }

    @Test
    public void unloadFlightCrew() throws Exception {
        Airport airport = new Airport("Zürich", 2);
        PassengerFlight passengerFlight = spy(getPassengerDreamlinerStatus(2, AIRPLANE_LANDED));
        airport.getFlights().add(passengerFlight);

        airport.unloadFlightCrew(2);

        verify(passengerFlight).unloadFlightCrew();
        assertThat(passengerFlight.getStatus(), is(EMPTY_AIRPLANE));
    }

    private void addPassengerToAirport(Airport airport, int anzPassenger) {
        for (int i = 0; i < anzPassenger; i++) {
            airport.acceptNewPassenger(new Passenger(RandomNameGenerator.generateName()));
        }
    }

    private void addFlightCrewToAirport(Airport airport) {
        airport.acceptNewPilot(pilot);
        airport.acceptNewCabinCrewMember(member);
        airport.acceptNewCabinCrewMember(member2);
        airport.acceptNewCabinCrewMember(member3);
        airport.acceptNewCabinCrewMember(member4);
        airport.acceptNewCabinCrewMember(member5);
    }


}