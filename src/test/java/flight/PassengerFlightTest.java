package flight;


import org.hamcrest.Matchers;
import org.junit.Test;
import person.Passenger;
import person.Person;

import java.util.ArrayList;
import java.util.List;

import static fixtures.Fixtures.getPassengerDreamlinerStatus;
import static fixtures.Fixtures.getPassengerDreamlinerStatusWithPassenger;
import static flight.Flight.CABIN_CREW_ON_BOARD;
import static flight.Flight.EMPTY_AIRPLANE;
import static flight.PassengerFlight.PASSENGER_NOT_ON_BOARD;
import static flight.PassengerFlight.PASSENGER_ON_BOARD;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Created by grazi on 15.05.16.
 */
public class PassengerFlightTest {

    @Test
    public void shouldNotBoardingPassengers_cabinCrewNotOnBoard() throws Exception {
        PassengerFlight flight = getPassengerDreamlinerStatus(1, EMPTY_AIRPLANE);
        Passenger p = new Passenger("Hans");
        List<Passenger> passengers = new ArrayList<Passenger>();
        passengers.add(p);

        flight.boardingPassengers(passengers);

        assertThat(flight.getPassengerStatus(), is(PASSENGER_NOT_ON_BOARD));
        assertThat(flight.passengers, is(not(contains(p))));
    }

    @Test
    public void shouldBoardingPassengers_cabinCrewOnBoard() throws Exception {
        PassengerFlight flight = getPassengerDreamlinerStatus(1, CABIN_CREW_ON_BOARD);
        Passenger p = new Passenger("Hans");
        List<Passenger> passengers = new ArrayList<Passenger>();
        passengers.add(p);

        flight.boardingPassengers(passengers);

        assertThat(flight.getPassengerStatus(), is(PASSENGER_ON_BOARD));
        assertThat(flight.passengers, contains(p));
    }

    @Test
    public void shouldUnloadPassengers() throws Exception {
        PassengerFlight flight = getPassengerDreamlinerStatusWithPassenger(1, CABIN_CREW_ON_BOARD, 5);
        List<Person> persons = flight.unloadPassengers();

        assertThat(persons.size(), is(5));
        assertThat(flight.passengers.isEmpty(), is(true));
    }
}