package factory;

import flight.Flight;

/**
 * Created by grazi on 11.05.16.
 */
public abstract class FlightFactory {

    protected int flightNumber;

    public Flight buildANewAirplane(String flightType) {

        if ("A380".equals(flightType) || "Dreamliner".equals(flightType)
                || "dreamliner".equals(flightType)) {
            return newInstanceOfFlight(count(), flightType);
        }

        return null;

    }

    protected abstract int count ();

    protected abstract Flight newInstanceOfFlight(int flightNumber, String flightType);


}
