package airport;

import flight.CargoFlight;
import flight.Flight;
import flight.PassengerFlight;
import person.CabinCrewMember;
import person.Passenger;
import person.Person;
import person.Pilot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static flight.CargoFlight.CARGO_LOADED;
import static flight.Flight.*;
import static flight.PassengerFlight.PASSENGER_ON_BOARD;

/**
 * Created by grazi on 28.04.16.
 */
public class Airport {

    private final int capacity;
    private List<Flight> flights = new ArrayList<Flight>();
    private List<Person> persons = new ArrayList<Person>();
    private String location;

    public Airport(String location, int capacity) {
        this.location = location;
        this.capacity = capacity;
    }

    public void acceptNewBuildedFlight(Flight airplane) {
        if (capacity > flights.size() && airplane.getStatus() == EMPTY_AIRPLANE) {
            flights.add(airplane);
        }
    }

    private Flight getFlight (int flightNumber) {
        for (Flight flight : getFlights()) {
            if (flight.getFlightNumber() == flightNumber) {
                return flight;
            }
        }
        return null;
    }

    public void boardingFlightCrew(int flightNumber) {
        Flight flight = getFlight(flightNumber);

        if (flight != null &&
                flight.getStatus() == Flight.EMPTY_AIRPLANE) {
            if (!getPilots().isEmpty() && getCabinCrewMembers().size() > 4) {
                Pilot pilot = getPilots().iterator().next();
                List<CabinCrewMember> members = getCabinCrewMembers().subList(0, 5);

                if (this.persons.remove(pilot) && this.persons.removeAll(members)) {
                    ArrayList<Person> flightCrew = new ArrayList<Person>();
                    flightCrew.add(pilot);
                    flightCrew.addAll(members);
                    flight.boardingFlightCrew(flightCrew);
                }
            } else {
                System.out.println("Sorry not enough Flight Crew on this Airport!!");
            }
        }

    }

    private List<Integer> getFlightsWithStatus (int status) {
        List<Integer> flightNumbers = new ArrayList<Integer>();
        for (Flight flight : getFlights()) {
            if (flight.getStatus() == status) {
                flightNumbers.add(flight.getFlightNumber());
            }
        }
        return flightNumbers;
    }

    public List<Integer> getFlightsWithoutFlightCrew() {
        return getFlightsWithStatus(Flight.EMPTY_AIRPLANE);
    }

    private List<Integer> getFlightsWithCargoStatus (int cargoStatus) {
        List<Integer> flightNumbers = new ArrayList<Integer>();
        for (Flight flight : getFlights()) {
            if (flight.getStatus() == Flight.CABIN_CREW_ON_BOARD && flight instanceof  CargoFlight) {
                CargoFlight cargoFlight = (CargoFlight) flight;
                if (cargoFlight.getCargoStatus() == cargoStatus) {
                    flightNumbers.add(cargoFlight.getFlightNumber());
                }
            }
        }
        return flightNumbers;
    }

    public List<Integer> getCargoFlights4LoadCargo() {
        return getFlightsWithCargoStatus(CargoFlight.CARGO_NOT_LOADED);
    }

    public void loadCargo4Flight(Integer flightNumber) {
        Flight flight = getFlight(flightNumber);
        CargoFlight cargoFlight = (CargoFlight) flight;
        cargoFlight.addCargo();
    }

    public List<Integer> getPassengerFlights4LoadPassenger() {
        return getFlightsWithStatus(CABIN_CREW_ON_BOARD);
    }

    public void loadPassenger4Flight(Integer flightNumber, int anzPassenger) {
        Flight flight = getFlight(flightNumber);
        if (flight.getStatus() == CABIN_CREW_ON_BOARD && getPassengers().size() >= anzPassenger) {
            PassengerFlight passengerFlight = (PassengerFlight) flight;
            List<Passenger> passengerToBoardIn = getPassengers().subList(0, anzPassenger);
            if (persons.removeAll(passengerToBoardIn)) {
                passengerFlight.boardingPassengers(passengerToBoardIn);
            }
        }
    }

    public List<Integer> getFlights4TakeOff() {

        return null;
    }

    public void takeOffFlight(Integer flightNumber) {
        Flight flight = getFlight(flightNumber);
        if (flight.getDestAirport() != null) {
            Airport destAirport = flight.getDestAirport();
            flight.takeOff ();
            if (destAirport.request4Tracking(flight)) {
                flights.remove(flight);
            }

        }
    }

    private boolean request4Tracking(Flight flight) {
        if (flight.getStatus() == AIRPLANE_ON_AIR) {
            flights.add(flight);
            return true;
        }
        return false;
    }

    public List<Integer> getFlightsWithoutDestination() {
        List<Integer> flightNumbers = new ArrayList<Integer>();
        for (Flight flight : getFlights()) {
            if (flight instanceof PassengerFlight) {
                PassengerFlight passengerFlight = (PassengerFlight) flight;
                if (passengerFlight.getPassengerStatus() == PASSENGER_ON_BOARD && passengerFlight.getDestAirport() == null) {
                    flightNumbers.add(passengerFlight.getFlightNumber());
                }
            }
        }
        return flightNumbers;
    }

    public void setDestination(Integer flightNumber, Airport destAirport) {
        Flight flight = getFlight(flightNumber);

        if (arePassengerOnBoard(flight) || isCargoLoaded(flight)) {
            flight.setDestAirport(destAirport);
        }
    }

    private boolean isCargoLoaded(Flight flight) {
        return flight instanceof CargoFlight && ((CargoFlight) flight).getCargoStatus() == CARGO_LOADED;
    }

    private boolean arePassengerOnBoard(Flight flight) {
        return flight instanceof PassengerFlight && ((PassengerFlight) flight).getPassengerStatus() == PASSENGER_ON_BOARD;
    }

    public List<Integer> getFlights4Landing() {
        return getFlightsWithStatus(AIRPLANE_ON_AIR);

    }

    public void landingFlight(Integer flightNumber) {
        Flight flight = getFlight(flightNumber);
        if (flight != null) {
            flight.land();
        }
    }

    public List<Integer> getFlights4GoToGate() {
        List<Integer> flightNumbers = new ArrayList<Integer>();
        for (Flight flight : getFlights()) {
            if (flight instanceof PassengerFlight &&
                    flight.getStatus() == AIRPLANE_LANDED) {
                flightNumbers.add(flight.getFlightNumber());
            }
        }
        return flightNumbers;
    }

    public void unloadPassengers(Integer flightNumber) {
        Flight flight = getFlight(flightNumber);
        if (flight != null) {
            PassengerFlight passengerFlight = (PassengerFlight) flight;
            persons.addAll(passengerFlight.unloadPassengers());
        }
    }

    public List<Integer> getFlights4GoToUnloadCargo() {
        List<Integer> flightNumbers = new ArrayList<Integer>();
        for (Flight flight : getFlights()) {
            if (flight instanceof CargoFlight &&
                    flight.getStatus() == AIRPLANE_LANDED) {
                CargoFlight cargoFlight = (CargoFlight) flight;
                if (cargoFlight.getCargoStatus() == CARGO_LOADED) {
                    flightNumbers.add(cargoFlight.getFlightNumber());
                }
            }
        }
        return flightNumbers;
    }

    public void unloadCargo(Integer flightNumber) {

    }

    public List<Integer> getFlights4GoToUnloadFlightCrew() {
        List<Integer> flightNumbers = new ArrayList<Integer>();
        for (Flight flight : getFlights()) {
            if (flight.getStatus() == AIRPLANE_LANDED &&
                    (!arePassengerOnBoard(flight) && !isCargoLoaded(flight))) {
                flightNumbers.add(flight.getFlightNumber());
            }
        }
        return flightNumbers;
    }

    public void unloadFlightCrew(Integer flightNumber) {
        Flight flight = getFlight(flightNumber);
        if (flight != null) {
            persons.addAll(flight.unloadFlightCrew());
        }
    }

    public void acceptNewPilot(Pilot pilot) {
        persons.add(pilot);

    }


    public void acceptNewCabinCrewMember(CabinCrewMember cabinCrewMember) {
        persons.add(cabinCrewMember);
    }

    public void acceptNewPassenger(Passenger passenger) {
        persons.add(passenger);
    }


    List<Flight> getFlights() {
        return flights;
    }


    private<T extends  Person> List<T> filterSubclassTypeOfPersons (List<Person> persons, Class<T> expectedClass) {
        List<T> subclassOfPersonList = new ArrayList<T>();
        for (Person person : persons) {
            if (person.getClass().equals(expectedClass)) {
                subclassOfPersonList.add((T) person);
            }
        }
        return subclassOfPersonList;
    }

    List<Pilot> getPilots() {
        return filterSubclassTypeOfPersons(persons, Pilot.class);
    }

    List<CabinCrewMember> getCabinCrewMembers() {
        return filterSubclassTypeOfPersons(persons, CabinCrewMember.class);
    }

    List<Passenger> getPassengers() {
        return filterSubclassTypeOfPersons(persons, Passenger.class);
    }


    public String getLocation() {
        return location;
    }
}
