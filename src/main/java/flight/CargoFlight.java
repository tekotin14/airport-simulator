package flight;


/**
 * Created by grazi on 28.04.16.
 */
public class CargoFlight extends Flight {

    public static final int CARGO_NOT_LOADED = 2;
    public static final int CARGO_LOADED = 3;

    protected int cargoStatus = CARGO_NOT_LOADED;

    public CargoFlight (int flightNumber, String flightType) {
        super (flightNumber, flightType);
    }

    public void addCargo() {
        if (status == CABIN_CREW_ON_BOARD) {
            cargoStatus = CARGO_LOADED;
        }
    }

    public Integer getCargoStatus () {
        return cargoStatus;
    }

}
